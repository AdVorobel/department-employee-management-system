# Introduction
Hello, everybody!

This is a small management app, which allows you easy to manage difficult company structures - departments.
You can simply add/delete/read about/update both departments and employees in it.
Also you will be able to see average salary of each department, search employees by date of birth inside a department 
and moreover system contains separate search be name, on the separate page.

If you're interested in - feel free to download my app :)

In next chapter you'll told how to correctly install and setup ManageSystem app.

# Quick start
There are 2 ways to download app on your PC:
* simply download from gitlab
* from `tar.gz` archive

##### ! First of all you need to have Python installed on your PC.
## Download
### Gitlab
* In the right corner, above files, find download button and choose most convenient extension according to your OS.
* Unpack archive into separate directory
### `tar.gz` archive (version is not up to date)
* Go to [link](https://drive.google.com/drive/folders/1lXD-XVLjAp_YIJoittu40Ffmed5Dor7w?usp=sharing) and download all 
files from it
* Put files in the separate directory
* Open the command prompt from this directory and type `tar -xvzf ManageSystem-1.0.0.tar.gz`
. You will see new directory `ManageSystem-1.0.0`
## Install
First you need to create virtual environment. In the root directory open command prompt and type:
*  `python -m venv [name of directory, usually venv or env]`
* `cd [name of directory]`
* `cd Scripts` on Windows, `cd bin` on Linux
* `. activate`

Next step return to the root directory and type in the command prompt:
* `python setup.py install`

## Setup
In `server-api` directory open file `db.py` and change variable DB_CONN so it has to look like -> "mysql+pymysql://`username`:`password`@localhost:`port`", 
where `username` is your MySQL Server username, `password` - your MySQL Server password, `port` - port of your MySQL Server connection.
##### ! If you don't have MySQL Server on your PC please install it.
Then go to the directory `server-api`, open command prompt and type `run`. Look at host name (it is between `http://` and `:5000`), 
copy it and paste into `server-api/static/scripts/host_.js` into variable `host_` so it will look like `const host_ = '[your pasted text here]'`.

Now terminate process by keyboard combination CTRL+C, and type again `run`. That's it :)

Enjoy!
