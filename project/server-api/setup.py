from setuptools import setup, find_packages

setup(
    name='ManageSystem',
    version='1.0.0',
    description='Simple system for management of departments and employees in it',
    author='Adrian Vorobel',
    author_email='adrianvorobel@gmail.com',
    url='https://gitlab.com/AdVorobel/department-employee-management-system/-/tree/master/project/server-api',
    packages=find_packages(),
    install_requires=[
        'Flask==1.1.2',
        'Flask-RESTful==0.3.8',
        'PyMySQL==0.10.1',
        'SQLAlchemy==1.3.20',
        'waitress==1.4.4',
    ]
)
