"""
Main module of application
"""
from datetime import date
import logging
from flask import Flask
from flask.json import JSONEncoder
from flask_restful import Api
from paste.translogger import TransLogger
from waitress import serve
from service import departments_apis, employees_apis
from views import controller
from db import create_db_if_not_exists

open('app.log', 'w').close()
logger = logging.getLogger('waitress')
logger.setLevel(logging.DEBUG)
logging.basicConfig(filename='app.log', level=logging.DEBUG)

create_db_if_not_exists()

app = Flask(__name__)
api = Api(app)

api.add_resource(employees_apis.EmployeeApi, '/api/employee/<emp_id>')
api.add_resource(employees_apis.EmployeeApiList, '/api/employees')
api.add_resource(employees_apis.EmployeeDepartmentApiList, '/api/employees/from/<dep_id>')
api.add_resource(departments_apis.DepartmentApi, '/api/department/<dep_id>')
api.add_resource(departments_apis.DepartmentApiList, '/api/departments')


class CustomJSONEncoder(JSONEncoder):  # pylint: disable=arguments-differ
    """
    custom encoder for date
    """

    def default(self, obj):
        """
        default method
        :param obj:
        :return:
        """
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


app.json_encoder = CustomJSONEncoder


@app.route('/')
def main_page():
    """
    main page
    :return:
    """
    return controller.main_page()


@app.route('/departments')
def departments_page():
    """
    all departments page
    :return:
    """
    return controller.departments()


@app.route('/departments/<id_>')
def department_page(id_):  # pylint: disable=unused-argument
    """
    department page
    :return:
    """
    return controller.department()


@app.route('/employee/<id_>')
def employee_page(id_):  # pylint: disable=unused-argument
    """
    employee page
    :return:
    """
    return controller.employee()


@app.route('/employees')
def employees_page():
    """
    all employees page
    :return:
    """
    return controller.employees()


if __name__ == "__main__":
    serve(TransLogger(app, setup_console_handler=True), listen='*:5000')
    # app.run(debug=True)
