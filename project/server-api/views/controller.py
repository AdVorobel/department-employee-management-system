"""
views control module
"""
from flask import render_template


def main_page():
    """
    main page controller
    :return: view
    """
    return render_template('main.html')


def departments():
    """
    departments page controller
    :return: view
    """
    return render_template('departments.html')


def department():
    """
    single department page controller
    :return: view
    """
    return render_template('department.html')


def employee():
    """
    single employee page
    :return:
    """
    return render_template('employee.html')


def employees():
    """
    all employee page
    :return:
    """
    return render_template('employees.html')
