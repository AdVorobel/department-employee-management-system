"""
CRUD operations for db items
"""
import re
from flask import jsonify
from flask_restful import abort
from sqlalchemy import exc
from sqlalchemy.orm import sessionmaker
from models.b_department import Department
from models.a_employee import Employee
from db import engine

Session = sessionmaker(bind=engine)


def validate_department(data):
    """
        validates data for department
        :param data: dict
        :return: bool
    """
    if not (2 <= len(data['name']) <= 35 and re.match(r'^[A-za-z0-9]{2,35}$', data['name'])):
        abort(406, message='Department name should be [2-35] symbols, [A-Za-z] characters, like examp. NewDepartment')
    if not len(data['description']) <= 256:
        abort(406, message='Department description should be [0-256] symbols')


def validate_employee(data):
    """
    validates data for employee
    :param data: dict
    :return: bool
    """
    if not (3 <= len(data['name']) <= 50 and re.match(r'^[A-z][a-z]{1,}\s[A-z][a-z]{1,}$', data['name'])):
        abort(406, message='Employee name should be [2-35], symbols [A-Za-z] characters, like examp. Name Surname')
    if not len(data['bio']) <= 256:
        abort(406, message='Employee bio should be [0-256] symbols')
    if not 1 <= len(data['role']) <= 30:
        abort(406, message='Employee role should be [1-30] symbols')
    if not (5 <= len(data['email']) <= 50 and re.match(r'[^@]+@[^@]+\.[^@]+',
                                                       data['email'])):
        abort(406, message='Employee email should be [5-50] symbols, like examp. some@email.end')
    if not (3 <= len(data['phone']) <= 50 and re.match(r'^\+[0-9]{12,14}$', data['phone'])):
        abort(406, message='Employee phone should be [13-15] symbols, like examp. +1234567890123')


def initialize_department(data, department=None):
    """
    initialize department
    :param department:
    :param data: dict
    :return: Department
    """
    depart = Department() if not department else department
    depart.name = data['name']
    depart.description = data['description']
    return depart


def initialize_employee(data, emp=None):
    """
        initialize employee
        :param emp:
        :param data: dict
        :return: Employee
    """
    employee = Employee() if not emp else emp
    employee.name = data['name']
    employee.bio = data['bio']
    employee.role = data['role']
    employee.salary = data['salary']
    employee.birthdate = data['birthdate']
    employee.address = data['address']
    employee.email = data['email']
    employee.phone = data['phone']
    employee.department_id = data['department_id']
    return employee


def add(data, item_type):  # pylint: disable=expression-not-assigned
    """
    adding item to db
    :param data: dict
    :param item_type: str
    :return: tuple
    """
    validate_department(data) if item_type == 'department' else validate_employee(data)
    new_item = initialize_department(data) if item_type == 'department' \
        else initialize_employee(data)

    session = Session()
    session.add(new_item)
    try:
        session.commit()
    except (exc.IntegrityError, exc.OperationalError, exc.DataError) as error:
        error = str(error.__dict__['orig'])
        abort(409, message=error)
    except exc.ArgumentError as error:
        abort(409, message=str(error))
    finally:
        session.close()

    return {'message': 'created'}, 201


def update(id_, data, item_type):  # pylint: disable=expression-not-assigned
    """
    update db item
    :param id_: int
    :param data: dict
    :param item_type: str
    :return: tuple
    """
    validate_department(data) if item_type == 'department' else validate_employee(data)
    table = Department if item_type == 'department' else Employee

    session = Session()
    item = session.query(table).filter_by(id=id_).first()
    if not item:
        abort(406, message='No such element in database (wrong id given)')

    item = initialize_department(data, item) if item_type == 'department' \
        else initialize_employee(data, item)

    try:
        session.commit()
    except (exc.IntegrityError, exc.IntegrityError, exc.DataError) as error:
        error = str(error.__dict__['orig'])
        abort(409, message=error)

    return jsonify({'data': item.to_dict()})


def get(id_, item_type):
    """
    get one db item
    :param id_: int
    :param item_type: str
    :return: tuple
    """
    table = Department if item_type == 'department' else Employee
    session = Session()
    item = session.query(table).filter_by(id=id_).first()
    if not item:
        abort(406, message='No such element in database (wrong id given)')
    session.close()

    return jsonify({"data": item.to_dict()})


def get_by_department(dep_id):
    """
    return all employee from particular department
    :param dep_id:
    :return:
    """
    session = Session()
    items = session.query(Employee).filter_by(department_id=dep_id).all()
    session.close()

    return jsonify({'data': [item.to_dict() for item in items]})


def get_all(item_type):
    """
    get all db item form one table
    :param item_type: str
    :return: tuple
    """
    table = Department if item_type == 'department' else Employee
    session = Session()
    items = session.query(table).all()
    salaries = {}
    if item_type == 'department':
        avg_salaries = session.execute("select d.name as name, avg(e.salary) as avg_salary from department d join "
                                       "(select department_id, salary from employee join department "
                                       "on department_id = department.id) e on d.id = e.department_id "
                                       "group by d.name;")
        for row in avg_salaries:
            name, salary = None, None
            for column, value in row.items():
                if column == 'name':
                    name = value
                elif column == 'avg_salary':
                    salary = value
            salaries[name] = float(salary)

    session.close()

    if salaries:
        return jsonify({'data': {'deps': [item.to_dict() for item in items], 'avg': salaries}})
    return jsonify({'data': [item.to_dict() for item in items]})


def delete(id_, item_type):
    """
    delete one db item
    :param id_: int
    :param item_type: str
    :return: tuple
    """
    table = Department if item_type == 'department' else Employee
    session = Session()

    item = session.query(table).filter_by(id=id_).first()
    if not item:
        abort(406, message='No such element in database (wrong id given)')

    session.delete(item)
    session.commit()

    return {'message': 'deleted'}, 200
