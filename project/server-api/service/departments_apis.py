"""
api handlers for department
"""
from flask_restful import Resource, reqparse
from service import crud_operations

department_args = reqparse.RequestParser()
department_args.add_argument('name', type=str, help='name of the department is required', required=True)
department_args.add_argument('description', type=str, help='description of the department is required')


class DepartmentApi(Resource):
    """
    api handlers with id parameter
    """
    @staticmethod
    def get(dep_id):
        """
            get one department
            :param dep_id: int
            :return: tuple
        """
        return crud_operations.get(dep_id, 'department')

    @staticmethod
    def put(dep_id):
        """
            update department
            :param dep_id: int
            :return: tuple
        """
        data = department_args.parse_args()
        return crud_operations.update(dep_id, data, 'department')

    @staticmethod
    def delete(dep_id):
        """
            delete one department
            :param dep_id: int
            :return: tuple
        """
        return crud_operations.delete(dep_id, 'department')


class DepartmentApiList(Resource):
    """
    api handlers without parameters
    """

    @staticmethod
    def get():
        """
            get all departments
            :return: tuple
            """
        return crud_operations.get_all('department')

    @staticmethod
    def post():
        """
            adding department
            :return: tuple
        """
        data = department_args.parse_args()
        return crud_operations.add(data, 'department')
