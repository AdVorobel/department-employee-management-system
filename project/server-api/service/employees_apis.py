"""
api handlers for employee
"""
from flask_restful import Resource, reqparse
from service import crud_operations

employee_args = reqparse.RequestParser()
employee_args.add_argument('name', type=str, help='Employee\'s name is required', required=True)
employee_args.add_argument('bio', type=str, help='Employee\'s bio')
employee_args.add_argument('role', type=str, help='Employee\'s role is required', required=True)
employee_args.add_argument('salary', type=int, help='Employee\'s salary is required', required=True)
employee_args.add_argument('birthdate', type=str, help='Employee\'s date of birth is required', required=True)
employee_args.add_argument('address', type=str, help='Employee\'s address')
employee_args.add_argument('email', type=str, help='Employee\'s email is required', required=True)
employee_args.add_argument('phone', type=str, help='Employee\'s phone is required', required=True)
employee_args.add_argument('department_id', type=int, help='Employee\'s department is required', required=True)


class EmployeeApi(Resource):
    """
        api handlers with id parameter
    """

    @staticmethod
    def get(emp_id):
        """
            get one employee
            :param emp_id: int
            :return: tuple
        """
        return crud_operations.get(emp_id, 'employee')

    @staticmethod
    def put(emp_id):
        """
            update employee
            :param emp_id: int
            :return: tuple
        """
        data = employee_args.parse_args()
        return crud_operations.update(emp_id, data, 'employee')

    @staticmethod
    def delete(emp_id):
        """
            delete one employee
            :param emp_id: int
            :return: tuple
        """
        return crud_operations.delete(emp_id, 'employee')


class EmployeeApiList(Resource):
    """
        api handlers without parameters
    """

    @staticmethod
    def get():
        """
            get all employees
            :return: tuple
            """
        return crud_operations.get_all('employee')

    @staticmethod
    def post():
        """
            adding employee
            :return: tuple
        """
        data = employee_args.parse_args()
        return crud_operations.add(data, 'employee')


class EmployeeDepartmentApiList(Resource):
    """
    api handlers for employees from departments
    """
    @staticmethod
    def get(dep_id):
        """
            get all employees from particular department
            :return: tuple
            """
        return crud_operations.get_by_department(dep_id)
