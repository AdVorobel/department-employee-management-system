
console.log('employee');

async function fetch_emps() {
    return await fetch(hostname+'/api/employees').then(response => {
        return response.json()
    }).then(response => {
        let data = response.data;
        let parent = document.getElementById('emp-list');
        for (let item of data) {
            let child = `<div class="emp-item d-flex flex-row align-items-center">
                    <a class="list-group-item list-group-item-action d-flex justify-content-between"
                       href="/employee/${window.btoa(item.id)}">${item.name}
                    </a>
                    <button data-id=${item.id} type="button" class="close ml-1 close-btn" aria-label="Close" 
                    data-toggle="modal" data-target="#removeEmployeeBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>`;
            parent.insertAdjacentHTML('beforeend', child)
        }
    }).catch(error => {
        console.log(error)
    });
}

fetch_emps().then(r => r);

function search() {
    let input, filter, emplist, elems, a, txtValue;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    emplist = document.getElementById("emp-list");
    elems = emplist.getElementsByClassName("emp-item");
    for (let i = 0; i < elems.length; i++) {
        a = elems[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            elems[i].classList.add('d-flex');
        } else {
            elems[i].classList.remove('d-flex');
            elems[i].classList.add('d-none');
        }
    }
}

window.onload = function () {
    $(document).on("click", ".close-btn", function () {
        let empId = $(this).data('id');
        $(".modal-footer #remove").val(empId);
    });

    document.getElementById('remove').addEventListener('click', async function (e) {
        let response = await fetch(hostname+`/api/employee/${e.target.value}`, {
            method: "DELETE"
        });
        if (response.ok) {
            window.location.reload()
        } else {
            alert(response.json().message)
        }
    });
};
