console.log('employee');
let url = window.location.href;
let id = url.substr(url.lastIndexOf('/') + 1);
let emp_data;

async function fetch_emp() {
    return await fetch(hostname + `/api/employee/${window.atob(id)}`, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return response.json()
    }).then(async response => {
        let data = response.data;
        let depart = 'no department';
        if (data.department_id) {
            depart = await fetch(hostname + `/api/department/${data.department_id}`).then(response => {
                return response.json()
            }).then(response => {
                return response.data.name
            });
        }
        let parent = document.getElementById('employee-info');
        let child = `<h5 class="card-title">${data.name}</h5>
                <p class="card-text">${data.bio}</p>
                <p class="card-text"><strong>Department: </strong>${depart} <button data-id="${data.id}" type="button" 
                class="move-btn btn btn-dark ml-2" data-toggle="modal"
                                data-target="#moveEmployee">move to</button></p>
                <p class="card-text"><strong>Role: </strong>${data.role}</p>
                <p class="card-text"><strong>Salary: </strong>${data.salary}$</p>
                <p class="card-text"><strong>Date of birth: </strong>${data.birthdate}</p>
                <p class="card-text"><strong>Address: </strong>${data.address ? data.address : 'Not provided'}</p>
                <p class="card-text"><strong>E-mail: </strong><a href="mailto: ${data.email}">${data.email}</a></p>
                <p class="card-text"><strong>Phone: </strong><a href="tel: ${data.phone}">${data.phone}</a></p>`;
        parent.insertAdjacentHTML('beforeend', child);
        emp_data = data;
    }).catch(error => {
        console.log(error)
    });
}

fetch_emp().then(response => {
    console.log('emp fetched')
});

window.onload = function () {
    document.getElementById('edit-btn').addEventListener('click', function (e) {
        document.getElementById('formGroupExampleInput').value = emp_data.name.split(' ')[0];
        document.getElementById('formGroupExampleInput1').value = emp_data.name.split(' ')[1];
        document.getElementById('formGroupExampleInput2').innerText = emp_data.bio;
        document.getElementById('formGroupExampleInput4').value = emp_data.role;
        document.getElementById('salary').value = emp_data.salary;
        document.getElementById('formGroupExampleInput6').value = emp_data.email;
        document.getElementById('formGroupExampleInput7').value = emp_data.phone;
        document.getElementById('formGroupExampleInput8').value = emp_data.birthdate;
        document.getElementById('formGroupExampleInput9').value = emp_data.address;
    });

    document.getElementById('edit-employee-btn').addEventListener("click", async function (e) {
        let form = document.forms.update_employee;
        let form_data = new FormData(form);
        let data = {
            'name': form_data.get('name') + ' ' + form_data.get('surname'),
            'bio': form_data.get('bio'),
            'role': form_data.get('role'),
            'salary': form_data.get('salary'),
            'birthdate': form_data.get('birthdate'),
            'email': form_data.get('email'),
            'phone': form_data.get('phone'),
            'address': form_data.get('address'),
            'department_id': emp_data.department_id,
            'id': emp_data.id
        };
        let response = await fetch(hostname + `/api/employee/${emp_data.id}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        });
        if (response.ok) {
            window.location.reload()
        } else {
            let msg = await response.json();
            alert(msg.message)
        }
    });

    $(document).on("click", ".move-btn", async function () {
        let empId = $(this).data('id');
        $(".modal-footer #move").val(empId);
        let response = await fetch(hostname + '/api/departments').then(response => response.json())
            .then(response => {
                return response.data.deps
            });
        let dep_list = document.getElementById('dep-list');
        for (let item of response) {
            let child = `<option value="${item.id}">${item.name}</option>`;
            dep_list.insertAdjacentHTML('beforeend', child);
        }
    });

    document.getElementById('move').addEventListener("click", async function (e) {
        let select = document.getElementById('dep-list').value;
        if (Number(select) === -1) {
            alert("Please select a department");
        } else {
            let emp = await fetch(hostname + `/api/employee/${e.target.value}`).then(res => res.json())
                .then(res => res.data);
            emp.department_id = select;
            await fetch(hostname + `/api/employee/${e.target.value}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(emp)
            }).then(res => res.json()).then(res => {
                if (res.data) {
                    window.location.reload()
                } else {
                    alert(res.message)
                }
            })
        }
    })
};