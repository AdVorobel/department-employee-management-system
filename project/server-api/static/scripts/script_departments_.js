
console.log('departments');

async function fetch_deps() {
    return await fetch(hostname+'/api/departments', {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return response.json()
    }).then(response => {
        let data = response.data.deps;
        let salary = response.data.avg;
        let parent = document.getElementById('depart-list');
        for (let item of data) {
            let child = `<div class="d-flex flex-row align-items-center" >
                <a class="list-group-item list-group-item-action d-flex justify-content-between" 
                href="/departments/${window.btoa(item.id)}">
                <pre>${item.name}     [avarage salary: ${salary[item.name]?salary[item.name]+'$':'-----' }]</pre>
                </a>
                <button data-id=${item.id} type="button" class="close ml-1 close-btn" aria-label="Close" data-toggle="modal" 
                data-target="#removeDepartBtn">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>`;
            parent.insertAdjacentHTML('beforeend', child)
        }
        return data;
    }).catch(error => {
        console.log(error)
    });
}

fetch_deps().then(response => {
    // console.log(response);
    console.log('all went good')
});

window.onload = function () {
    console.log('loaded');
    document.getElementById('add_depart_btn').addEventListener('click', async function (event) {
        let form = document.forms.add_depart;
        let form_data = new FormData(form);
        let data = {
            'name': form_data.get('name'),
            'description': form_data.get('description')
        };
        await fetch(hostname+'/api/departments', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(async response => {
            return {response: await response.json(), status: response.status}
        }).then(response => {
            if (response.status !== 201) {
                alert(response.response.message);
            } else {
                window.location.reload()
            }
        })
    });

    document.getElementById('add_depart_cancel_btn').addEventListener('click', function () {
        document.getElementById('add_depart').reset()
    });

    $(document).on("click", ".close-btn", function () {
        let depId = $(this).data('id');
        $(".modal-footer #remove").val(depId);
    });

    document.getElementById('remove').addEventListener('click', async function (e) {
        let response = await fetch(hostname+`/api/department/${e.target.value}`, {
            method: "DELETE"
        });
        if(response.ok){
            window.location.reload()
        } else {
            alert(response.json().message)
        }
    })
};

