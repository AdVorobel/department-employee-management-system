console.log('department');
let url = window.location.href;
let id = url.substr(url.lastIndexOf('/') + 1);
let dep_data, emps_data;

async function fetch_dep_emps() {
    return await fetch(hostname + `/api/employees/from/${window.atob(id)}`, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return response.json()
    }).then(response => {
        let data = response.data;
        let parent = document.getElementById('table-data');
        for (let item in data) {
            let child = `<tr id="${data[item].id}">
                    <th scope="row">${Number(item) + 1}</th>
                    <td><a href="/employee/${window.btoa(data[item].id)}">${data[item].name}</a></td>
                    <td>${data[item].role}</td>
                    <td>${data[item].salary}$</td>
                    <td><a href="tel:${data[item].phone}">${data[item].phone}</a></td>
                    <td>
                        <button data-id="${data[item].id}" type="button" class="move-btn btn btn-dark" data-toggle="modal"
                                data-target="#moveEmployee">
                        move to
                        </button>
                        <button data-id="${data[item].id}" type="button" class="close close-btn" aria-label="Close" data-toggle="modal"
                                data-target="#removeEmployeeBtn">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </td>
                </tr>`;
            parent.insertAdjacentHTML('beforeend', child)
        }
        emps_data = data
    }).catch(error => {
        console.log(error)
    });
}

async function fetch_dep() {
    return await fetch(hostname + `/api/department/${window.atob(id)}`, {
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => {
        return response.json()
    }).then(response => {
        let data = response.data;
        document.getElementById('dep-header').innerText = data.name;
        document.getElementById('dep-description').innerText = data.description;
        dep_data = data;
    }).catch(error => {
        console.log(error)
    });
}

fetch_dep().then(response => {
    console.log('dep fetched')
});

fetch_dep_emps().then(response => {
    // console.log(response);
    console.log('emps fetched')
});

window.onload = function () {
    document.getElementById('add_employee_btn').addEventListener('click', async function (event) {
        let form = document.forms.add_employee;
        let form_data = new FormData(form);
        let data = {
            'name': form_data.get('name') + ' ' + form_data.get('surname'),
            'bio': form_data.get('bio'),
            'role': form_data.get('role'),
            'salary': form_data.get('salary'),
            'birthdate': form_data.get('birthdate'),
            'email': form_data.get('email'),
            'phone': form_data.get('phone'),
            'address': form_data.get('address'),
            'department_id': window.atob(id)
        };
        await fetch(hostname + '/api/employees', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(async response => {
            return {response: await response.json(), status: response.status}
        }).then(response => {
            if (response.status !== 201) {
                alert(response.response.message);
            } else {
                window.location.reload()
            }
        })
    });

    document.getElementById('add_employee_cancel_btn').addEventListener('click', function () {
        document.getElementById('add_employee').reset()
    });

    $(document).on("click", ".close-btn", function () {
        let empId = $(this).data('id');
        $(".modal-footer #remove").val(empId);
    });

    document.getElementById('remove').addEventListener('click', async function (e) {
        let response = await fetch(hostname + `/api/employee/${e.target.value}`, {
            method: "DELETE"
        });
        if (response.ok) {
            window.location.reload()
        } else {
            alert(response.json().message)
        }
    });

    document.getElementById('add_employee_cancel_btn').addEventListener('click', function () {
        document.getElementById('add_employee').reset()
    });

    document.getElementById('edit_dep_btn').addEventListener("click", function (e) {
        document.getElementById('formGroupExampleInput').value = dep_data.name;
        document.getElementById('formGroupExampleInput2').value = dep_data.description;
    });

    document.getElementById('edit_depart_btn').addEventListener('click', async function (e) {
        let form = document.forms.edit_depart;
        let form_data = new FormData(form);
        let data = {
            'name': form_data.get('name'),
            'description': form_data.get('description'),
            'id': dep_data.id
        };
        console.log(data);
        let response = await fetch(hostname + `/api/department/${dep_data.id}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        });
        if (response.ok) {
            window.location.reload()
        } else {
            alert(response.json().message)
        }
    });

    $(document).on("click", ".move-btn", async function () {
        let empId = $(this).data('id');
        $(".modal-footer #move").val(empId);
        let response = await fetch(hostname + '/api/departments').then(response => response.json())
            .then(response => {
                return response.data.deps
            });
        let dep_list = document.getElementById('dep-list');
        for (let item of response) {
            let child = `<option value="${item.id}">${item.name}</option>`;
            dep_list.insertAdjacentHTML('beforeend', child);
        }
    });

    document.getElementById('move').addEventListener("click", async function (e) {
        let select = document.getElementById('dep-list').value;
        if (Number(select) === -1) {
            alert("Please select a department");
        } else {
            let emp = await fetch(hostname + `/api/employee/${e.target.value}`).then(res => res.json())
                .then(res => res.data);
            emp.department_id = select;
            await fetch(hostname + `/api/employee/${e.target.value}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(emp)
            }).then(res => res.json()).then(res => {
                if (res.data) {
                    window.location.reload()
                } else {
                    alert(res.message)
                }
            })
        }
    })
};

function date_filter() {
    let from = document.getElementById("from-date").value;
    let to = document.getElementById("to-date").value;
    let table = document.getElementById("table-data");
    let elems = table.getElementsByTagName("tr");
    if (!to) {
        let date = new Date();
        to = `${String(date).split(' ')[3]}-${date.getMonth() + 1}-${String(date).split(' ')[2]}`
    }
    for (let i = 0; i < elems.length; i++) {
        let id = elems[i].getAttribute('id');
        let emp = emps_data.filter(item => Number(item.id) === Number(id))[0];
        if (emp.birthdate >= from && emp.birthdate <= to) {
            elems[i].classList.remove('d-none');
        } else {
            elems[i].classList.add('d-none');
        }
    }
}
