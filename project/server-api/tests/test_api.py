import os
import sys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

current_path = os.path.dirname(os.path.abspath(__file__))
ROOT_PATH = os.path.join(current_path, '..')
sys.path.append(ROOT_PATH)

from models import a_employee, b_department
import service.crud_operations as crud
from app import app

# pre-stage
test_engine = create_engine('sqlite://')
b_department.Base.metadata.create_all(bind=test_engine)
a_employee.Base.metadata.create_all(bind=test_engine)
TestSession = sessionmaker(bind=test_engine)

crud.Session = TestSession

department_body = {
    'name': "TestDepart",
    'description': "Test description"
}

employee_body = {
    'name': "New Employee",
    'bio': "",
    'role': 'Director',
    'salary': 575,
    'birthdate': None,
    # troubles with sqlite, it takes only Python date and datetime into field Date, but json converts them into str
    'address': '',
    'email': 'email@example.test',
    'phone': '+000000000000',
    'department_id': 1
}


def test_validation():
    assert crud.validate_department(department_body) is None
    assert crud.validate_employee(employee_body) is None


def test_initialize():
    assert isinstance(crud.initialize_department(department_body), b_department.Department)
    assert isinstance(crud.initialize_employee(employee_body), a_employee.Employee)


# departments api tests
def test_departments_post():
    response = app.test_client().post('/api/departments', json=department_body,
                                      content_type='application/json')
    assert response.status_code == 201
    assert response.json['message'] == 'created'


def test_departments_duplicate_item_post():
    response = app.test_client().post('/api/departments', json=department_body,
                                      content_type='application/json')
    assert response.status_code == 409


def test_departments_with_existing_name_post():
    response = app.test_client().post('/api/departments', json=department_body,
                                      content_type='application/json')
    assert response.status_code == 409


def test_departments_get_all():
    response = app.test_client().get('/api/departments')
    assert response.status_code == 200
    assert isinstance(response.json['data'], list)


def test_department_update_and_get():
    added = app.test_client().get('/api/department/1')
    assert added.status_code == 200
    assert added.json['data']['name'] == 'TestDepart'
    body = department_body
    body['name'] = 'UpdatedDepartmentName'
    response = app.test_client().put('/api/department/1', json=body,
                                     content_type='application/json')
    assert response.status_code == 200
    updated = app.test_client().get('/api/department/1')
    assert updated.status_code == 200
    assert updated.json['data']['name'] == 'UpdatedDepartmentName'


def test_department_delete():
    response = app.test_client().delete('/api/department/1')
    assert response.status_code == 200


def test_get_deleted_department():
    response = app.test_client().get('/api/department/1')
    assert response.status_code == 406


# employees api tests
def test_employees_post():
    response = app.test_client().post('/api/employees', json=employee_body, content_type='application/json')
    assert response.status_code == 201
    assert response.json['message'] == 'created'


def test_employees_get_all():
    response = app.test_client().get('/api/employees')
    assert response.status_code == 200
    assert isinstance(response.json['data'], list)


def test_get_employees_from_department():
    response = app.test_client().get('/api/employees/from/1')
    assert response.status_code == 200
    assert isinstance(response.json['data'], list)


def test_employees_update_and_get():
    added = app.test_client().get('/api/employee/1')
    assert added.status_code == 200
    assert added.json['data']['name'] == 'New Employee'
    body = employee_body
    body['name'] = 'Updated Name'
    response = app.test_client().put('/api/employee/1', json=body,
                                     content_type='application/json')
    assert response.status_code == 200
    updated = app.test_client().get('/api/employee/1')
    assert updated.status_code == 200
    assert updated.json['data']['name'] == 'Updated Name'


def test_employee_delete():
    response = app.test_client().delete('/api/employee/1')
    assert response.status_code == 200


def test_get_deleted_employee():
    response = app.test_client().get('/api/employee/1')
    assert response.status_code == 406
