from flask_testing.utils import TestCase
from app import app


class MyTest(TestCase):

    def create_app(self):
        return app

    def test_main(self):
        response = self.client.get('/')
        self.assert_template_used('main.html')
        self.assert_status(response, 200)

    def test_dep(self):
        response = self.client.get('/departments')
        self.assert_template_used('departments.html')
        self.assert_status(response, 200)

    def test_deps(self):
        response = self.client.get('/departments/id')
        self.assert_template_used('department.html')
        self.assert_status(response, 200)

    def test_emp(self):
        response = self.client.get('/employee/id')
        self.assert_template_used('employee.html')
        self.assert_status(response, 200)

    def test_emps(self):
        response = self.client.get('/employees')
        self.assert_template_used('employees.html')
        self.assert_status(response, 200)
