"""
ORM models Employee
"""
from sqlalchemy import Integer, String, Column, ForeignKey, Date
from sqlalchemy.ext.declarative import declarative_base
import models.b_department as department

Base = declarative_base()


class Employee(Base):  # pylint: disable=too-few-public-methods
    """
    Employee class
    """
    __tablename__ = 'employee'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(50), nullable=False)
    bio = Column('bio', String(256))
    role = Column('role', String(30), nullable=False)
    salary = Column('salary', Integer, nullable=False)
    birthdate = Column('birthdate', Date, nullable=True)
    address = Column('address', String(50), nullable=True)
    email = Column('email', String(50), unique=True, nullable=False)
    phone = Column('phone', String(15), unique=True, nullable=False)
    department_id = Column('department_id', Integer, ForeignKey(department.Department.id, ondelete="SET NULL"))

    def to_dict(self):
        """
        return object as dict
        :return: dict
        """
        return {
            'id': self.id,
            'name': self.name,
            'bio': self.bio,
            'role': self.role,
            'salary': self.salary,
            'birthdate': self.birthdate,
            'address': self.address,
            'email': self.email,
            'phone': self.phone,
            'department_id': self.department_id
        }
