"""
ORM model Department
"""
from sqlalchemy import Integer, String, Column
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Department(Base):  # pylint: disable=too-few-public-methods
    """
    Department class
    """
    __tablename__ = 'department'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', String(35), unique=True, nullable=False)
    description = Column('description', String(256))

    def to_dict(self):
        """
        Return object as dict
        :return: dict
        """
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }
