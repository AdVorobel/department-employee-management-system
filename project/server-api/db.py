"""
Initializing db and db session module
"""
import logging
from sqlalchemy import create_engine, exc
from models import a_employee, b_department

DB_CONN = 'mysql+pymysql://root:root@localhost:3306'


def create_db_if_not_exists():
    """
    creates database if it not exists
    :return:
    """
    try:
        db_check = create_engine(DB_CONN + '/departments', echo=True)
        db_check.connect()
    except exc.OperationalError:
        try:
            db_engine = create_engine(DB_CONN)
            connection = db_engine.connect()
            connection.execute("commit")
            connection.execute("create database if not exists departments")
            connection.close()
            db_en = create_engine(DB_CONN + '/departments', echo=True)
            b_department.Base.metadata.create_all(bind=db_en)
            a_employee.Base.metadata.create_all(bind=db_en)
        except exc.OperationalError:
            logging.log(logging.WARNING, 'cannot create connection to db: %s', DB_CONN)


def db_conn():
    """
    returns engine
    :return:
    """
    return create_engine(DB_CONN + '/departments', echo=True)


engine = db_conn()
